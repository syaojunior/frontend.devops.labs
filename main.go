package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

const (
	apiURL = "http://88.201.239.152:8080"
)

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Message struct {
	Recipient string `json:"recipient"`
	Content   string `json:"content,omitempty"`
	File      string `json:"file,omitempty"`
}

type UserSession struct {
	Username string
	Token    string
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Choose an option: [1] Sign In, [2] Sign Up")
	scanner.Scan()
	choice := scanner.Text()

	switch choice {
	case "1":
		session, err := signIn()
		if err != nil {
			fmt.Println("Sign in failed:", err)
			return
		}
		fmt.Printf("Welcome, %s!\n", session.Username)
		userActions(session)
	case "2":
		err := signUp()
		if err != nil {
			fmt.Println("Sign up failed:", err)
			return
		}
		fmt.Println("Registration successful, please sign in.")
	default:
		fmt.Println("Invalid option selected.")
	}
}

func signIn() (*UserSession, error) {
	credentials := promptCredentials()

	data, err := json.Marshal(credentials)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", apiURL+"/auth/signin", bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 403 {
		return nil, fmt.Errorf("invalid login credentials")
	} else if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("login failed with status: %d", resp.StatusCode)
	}

	// Определяем структуру для декодирования тела ответа
	var responseBody struct {
		Username string `json:"username"`
		Jwt      string `json:"jwt"`
	}

	// Декодируем тело ответа в структуру
	if err := json.NewDecoder(resp.Body).Decode(&responseBody); err != nil {
		return nil, fmt.Errorf("error decoding response: %v", err)
	}

	// Создаем и возвращаем объект UserSession
	session := &UserSession{
		Username: responseBody.Username,
		Token:    responseBody.Jwt,
	}
	fmt.Println("Token:", session.Token)
	return session, nil
}

func signUp() error {
	credentials := promptCredentials()

	data, err := json.Marshal(credentials)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", apiURL+"/auth/signup", bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("error creating request: %v", err)
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("registration failed with status: %d", resp.StatusCode)
	}

	return nil
}

func promptCredentials() Credentials {
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Print("Enter username: ")
	scanner.Scan()
	username := scanner.Text()

	fmt.Print("Enter password: ")
	scanner.Scan()
	password := scanner.Text()

	return Credentials{Username: username, Password: password}
}

func userActions(session *UserSession) {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Println("Choose action: [1] Send Message, [2] Send File, [3] ShowMessages, [4] Logout")
		scanner.Scan()
		action := scanner.Text()

		switch action {
		case "1":
			sendMessage(session)
		case "2":
			sendMessage(session, true)
		case "3":
			showMessage(session)
			return
		case "4":
			fmt.Println("Logging out...")
			return
		default:
			fmt.Println("Invalid action selected.")
		}
	}
}

func sendMessage(session *UserSession, isFile ...bool) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Enter recipient username: ")
	scanner.Scan()
	recipient := scanner.Text()

	var messageContent string
	if len(isFile) == 0 || !isFile[0] {
		fmt.Print("Enter message content: ")
		scanner.Scan()
		messageContent = scanner.Text()
	} else {
		fmt.Print("Enter file path: ")
		scanner.Scan()
		messageContent = scanner.Text()
	}

	message := Message{
		Recipient: recipient,
		Content:   messageContent,
	}

	data, err := json.Marshal(message)
	if err != nil {
		fmt.Println("Error encoding message:", err)
		return
	}

	var endpoint string
	if len(isFile) == 0 || !isFile[0] {
		endpoint = "/messages/"
	} else {
		endpoint = "/file/"
	}

	req, err := http.NewRequest("POST", apiURL+endpoint, bytes.NewBuffer(data))
	if err != nil {
		fmt.Println("Error creating request:", err)
		return
	}
	req.Header.Set("Authorization", "Bearer "+session.Token)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending request:", err)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Println("Request failed. Status:", resp.StatusCode)
		return
	}

	fmt.Println("Message sent successfully.")
}

func showMessage(session *UserSession) {
	req, err := http.NewRequest("GET", apiURL+"/messages/", nil)
	if err != nil {
		fmt.Println("Error creating request:", err)
		return
	}
	req.Header.Set("Authorization", "Bearer "+session.Token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error sending request:", err)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		fmt.Println("Request failed. Status:", resp.StatusCode)
		return
	} else if resp.StatusCode == http.StatusOK {
		bodyBytes, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		bodyString := string(bodyBytes)
		fmt.Println(bodyString)
	}
}
